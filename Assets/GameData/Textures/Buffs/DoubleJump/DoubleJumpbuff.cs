﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleJumpbuff : MonoBehaviour {

	private GameObject AS;
	public AudioClip PickUpSound;


	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			GlobalSettings.Score += 300;
			AS = GameObject.FindWithTag("Audio");
			AS.GetComponent<AudioSource>().PlayOneShot(PickUpSound);
			GlobalSettings.DoubleJumpBuff = true;
			CharacterController.JumpTimes = 2;
			Destroy(gameObject);
		}
	}

}
