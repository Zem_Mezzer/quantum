﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour {

	public Rigidbody2D rb;
	public AudioClip HitSound;
	private GameObject AS;
	public float speed;
	public bool isgreenbullet;
	void Start () {

		rb.velocity = transform.right * speed;
	
	}
	
	void OnTriggerEnter2D(Collider2D col)
	{

		if (col.tag == "Enemy")
		{
			AS = GameObject.FindWithTag("Audio");
			AS.GetComponent<AudioSource>().PlayOneShot(HitSound);
			
			Destroy(gameObject);
		}
		if (col.gameObject.layer == 8)
		{
			if (!isgreenbullet)
				Destroy(gameObject);
		}
		if (col.gameObject.layer == 9)
		{
			Destroy(gameObject);
		}
	}
	
}
