﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour {

	public GameObject Bullet;

	private float dalay;
	private float dalaytime=2;
	public AudioClip ShootSound;
	public GameObject Robo;

	void Start()
	{
		dalay = dalaytime;
	}

	void OnTriggerStay2D(Collider2D col)
	{
		if (col.tag == "Player"&&dalay<=0)
		{
			GameObject AS = GameObject.FindWithTag("Audio");
			AS.GetComponent<AudioSource>().PlayOneShot(ShootSound);
			Instantiate(Bullet, Robo.transform.position,Robo.transform.rotation);
			dalay = dalaytime;
		}
	}

	void Update()
	{
		if (dalay > 0)
		{
			dalay -= Time.deltaTime;
		}
	}
}
