﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour {

	public float speed;

	private Transform Player;
	private Vector2 target;

	void Start () {
		Player = GameObject.FindGameObjectWithTag("Player").transform;
		target = new Vector2(Player.position.x, Player.position.y);
	}
	
	void Update () {
		transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);
		//
		if (transform.position.x == target.x && transform.position.y == target.y)
		{
			Destroy(gameObject);
		}
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player"&&PlayerDamage.damagedalay<0)
		{
			GlobalSettings.HP--;
			PlayerDamage.TakeDamage = true;
			Destroy(gameObject);
		}
	}
}
