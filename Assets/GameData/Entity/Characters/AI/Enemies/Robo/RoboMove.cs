﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoboMove : MonoBehaviour {

	public GameObject FirstWayPoint;
	public GameObject SecondWayPoint;
	public float speed;
	private bool LeftOrRight = true;
	void Update () {
		if (LeftOrRight == true) {
			transform.position = Vector2.MoveTowards(transform.position, FirstWayPoint.transform.position, speed * Time.deltaTime);
			if (Vector2.Distance(transform.position, FirstWayPoint.transform.position) < 0.1f)
			{
				LeftOrRight = false;
				Flip();
			}
		}
		else
			if (LeftOrRight == false)
		{
			transform.position = Vector2.MoveTowards(transform.position, SecondWayPoint.transform.position, speed * Time.deltaTime);
			if (Vector2.Distance(transform.position, SecondWayPoint.transform.position) < 0.1f)
			{
				LeftOrRight = true;
				Flip();
			}
		}
	}

	
	void Flip()
	{
	   Vector3 Scaler = transform.localScale;
	   Scaler.x *= -1;
	   transform.localScale = Scaler;
	}
}
