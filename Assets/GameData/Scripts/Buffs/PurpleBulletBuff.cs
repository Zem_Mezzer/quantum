﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurpleBulletBuff : MonoBehaviour {

	public AudioClip PickUpSound;

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			GlobalSettings.PurpleBulletBuff = true;
			GlobalSettings.GreenBulletBuff = false;
			GlobalSettings.DefultBullet = false;
			GameObject AS = GameObject.FindWithTag("Audio");
			AS.GetComponent<AudioSource>().PlayOneShot(PickUpSound);
			GlobalSettings.Score += 300;
			Destroy(gameObject);
		}
	}
}
