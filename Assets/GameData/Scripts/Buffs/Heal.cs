﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : MonoBehaviour {

	public AudioClip PickUpSound;

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			GlobalSettings.Score += 300;
			if (GlobalSettings.HP < 3)
			{
				GlobalSettings.HP++;
				GameObject AS = GameObject.FindWithTag("Audio");
				AS.GetComponent<AudioSource>().PlayOneShot(PickUpSound);
				Destroy(gameObject);
			}
			else
			{
				GameObject AS = GameObject.FindWithTag("Audio");
				AS.GetComponent<AudioSource>().PlayOneShot(PickUpSound);
				Destroy(gameObject);
			}
		}
	}
}
