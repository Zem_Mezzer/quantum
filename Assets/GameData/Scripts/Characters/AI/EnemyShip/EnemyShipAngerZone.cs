﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShipAngerZone : MonoBehaviour {

	public AudioClip ShootSound;
	public GameObject Bullet;
	private float dalay;
	private int timetoshoot=2;
	public GameObject ShootPoint;

	void OnTriggerStay2D(Collider2D col)
	{
		if (col.tag == "Player"&&dalay<0)
		{
			GameObject AS = GameObject.FindWithTag("Audio");
			AS.GetComponent<AudioSource>().PlayOneShot(ShootSound);
			Instantiate(Bullet, ShootPoint.transform.position,ShootPoint.transform.rotation);
			dalay = timetoshoot;
		}
	}

	void Update()
	{
		dalay -= Time.deltaTime;
	}
}
