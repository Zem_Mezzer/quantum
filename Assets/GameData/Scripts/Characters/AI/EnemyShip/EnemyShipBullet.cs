﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShipBullet : MonoBehaviour {

	public Rigidbody2D rb;
	public float Speed;

	void Start()
	{
		rb.velocity = transform.right * Speed;
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player" && PlayerDamage.damagedalay < 0)
		{
			PlayerDamage.TakeDamage = true;
			GlobalSettings.HP--;
			Destroy(this.gameObject);
		}
		if (col.tag == "Bullet"||col.gameObject.layer==8)
		{
			Destroy(this.gameObject);
		}
	}
}
