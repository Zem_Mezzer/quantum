﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShipStartShoot : MonoBehaviour {

	public GameObject AngerZone;

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.layer == 9)
		{
			GetComponent<EnemyDamageCollider>().enabled=enabled;
			AngerZone.SetActive(true);
		}
	}
}
