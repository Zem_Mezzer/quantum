﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMove : MonoBehaviour {


	public GameObject FirstWayPoint;
	public GameObject SecondWayPoint;
	public float speed;
	private bool LeftOrRight = true;
	void Update()
	{
		if (LeftOrRight == true)
		{
			transform.position = Vector2.MoveTowards(transform.position, FirstWayPoint.transform.position, speed * Time.deltaTime);
			if (Vector2.Distance(transform.position, FirstWayPoint.transform.position) < 0.1f)
			{
			
				Flip();
			}
		}
		else
			if (LeftOrRight == false)
		{
			transform.position = Vector2.MoveTowards(transform.position, SecondWayPoint.transform.position, speed * Time.deltaTime);
			if (Vector2.Distance(transform.position, SecondWayPoint.transform.position) < 0.1f)
			{
				
				Flip();
			}
		}
	}


	void Flip()
	{
		LeftOrRight=!LeftOrRight;

		transform.Rotate(0f, 180f, 0f);
	}
}
