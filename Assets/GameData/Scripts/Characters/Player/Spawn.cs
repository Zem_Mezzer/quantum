﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {


	public GameObject Player;
	void _Spawn()
	{
		Player.GetComponent<SpriteRenderer>().enabled = enabled;
		Player.GetComponent<CharacterController>().enabled = enabled;
		Destroy(this.gameObject);
	}
}
