﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDamage : MonoBehaviour {

	private GameObject Heart1;
	private GameObject Heart2;
	private GameObject Heart3;
	private GameObject IronHeart;
	private GameObject GoldHeart;
	private GameObject GhostHeart;
	public static bool TakeDamage = false;
	public int debug;
	public static float damagedalay;
	private float dalaytodamage = 2;
	public static bool falldeath = false;
	public GameObject part;
	public AudioClip HitSound;
	public GameObject Shield;
	public static bool Heal;


	// Use this for initialization
	void Start () {
		damagedalay = -1;

		Heart1 = GameObject.FindWithTag("Heart1");
		Heart2 = GameObject.FindWithTag("Heart2");
		Heart3 = GameObject.FindWithTag("Heart3");
		IronHeart = GameObject.FindWithTag("IronHeart");
		GoldHeart = GameObject.FindWithTag("GoldHeart");
		GhostHeart = GameObject.FindWithTag("GhostHeart");

		if (GlobalSettings.HP == 3&&!GlobalSettings.Godmode)
		{
			Heart3.GetComponent<Image>().enabled = enabled;
			Heart2.GetComponent<Image>().enabled = enabled;
			Heart1.GetComponent<Image>().enabled = enabled;
		}
		else
		if (GlobalSettings.HP == 2&&!GlobalSettings.Godmode)
		{

			Heart2.GetComponent<Image>().enabled = enabled;
			Heart1.GetComponent<Image>().enabled = enabled;
		}
		else
		if (GlobalSettings.HP == 1&&!GlobalSettings.Godmode)
		{
			Heart1.GetComponent<Image>().enabled = enabled;
		}
		else
		if (GlobalSettings.Godmode)
		{
			GlobalSettings.HP = 9999999;
			IronHeart.GetComponent<Image>().enabled = enabled;
			GoldHeart.GetComponent<Image>().enabled = !enabled;
			GhostHeart.GetComponent<Image>().enabled = !enabled;
			Heart1.GetComponent<Image>().enabled = !enabled;
			Heart2.GetComponent<Image>().enabled = !enabled;
			Heart3.GetComponent<Image>().enabled = !enabled;
		}
	}
	
	void Update()
	{

		if (GlobalSettings.HP == 2)
		{
			Heart2.GetComponent<Image>().enabled = enabled;
		}
		if (GlobalSettings.HP == 3)
		{
			Heart2.GetComponent<Image>().enabled = enabled;
			Heart3.GetComponent<Image>().enabled = enabled;
		}

		Debug.Log( GlobalSettings.HP);
		if (GlobalSettings.HP == 2&&TakeDamage&&damagedalay<0)
		{
			damagedalay = dalaytodamage;
			GetComponent<AudioSource>().PlayOneShot(HitSound);
			Heart3.GetComponent<Image>().enabled = !enabled;
			TakeDamage = false;
			Shield.GetComponent<SpriteRenderer>().enabled = enabled;
			part.GetComponent<ParticleSystem>().Play();
			GlobalSettings.Score -= 500;
			if (GlobalSettings.Score < 0)
			{
				GlobalSettings.Score = 0;
			}
		}
		if (GlobalSettings.HP == 1 && TakeDamage&&damagedalay < 0)
		{
			damagedalay = dalaytodamage;
			GetComponent<AudioSource>().PlayOneShot(HitSound);
			Heart3.GetComponent<Image>().enabled = !enabled;
			Heart2.GetComponent<Image>().enabled = !enabled;
			TakeDamage = false;
			Shield.GetComponent<SpriteRenderer>().enabled = enabled;
			part.GetComponent<ParticleSystem>().Play();
			GlobalSettings.Score -= 500;
			if (GlobalSettings.Score < 0)
			{
				GlobalSettings.Score = 0;
			}
		}
		if (GlobalSettings.HP == 0 && TakeDamage&&damagedalay < 0)
		{
			GetComponent<AudioSource>().PlayOneShot(HitSound);
			Heart3.GetComponent<Image>().enabled = !enabled;
			Heart2.GetComponent<Image>().enabled = !enabled;
			Heart1.GetComponent<Image>().enabled = !enabled;
			GlobalSettings.Score -= 10000;
			if (GlobalSettings.Score < 0)
			{
				GlobalSettings.Score = 0;
			}
			Death.Death_=true;
			TakeDamage = false;
		}
		if (falldeath)
		{
			falldeath = false;
			Heart3.GetComponent<Image>().enabled = !enabled;
			Heart2.GetComponent<Image>().enabled = !enabled;
			Heart1.GetComponent<Image>().enabled = !enabled;
			GlobalSettings.Score -= 10000;
			if (GlobalSettings.Score < 0)
			{
				GlobalSettings.Score = 0;
			}
			Death.Death_ = true;
		}
		if (damagedalay > 0)
		{
			damagedalay -= Time.deltaTime;
			if (damagedalay < 0)
			{
				Shield.GetComponent<SpriteRenderer>().enabled = !enabled;
			}
		}
	}

}
