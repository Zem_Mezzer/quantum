﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Death : MonoBehaviour {

	public GameObject ps;
	public static bool Death_=false;
	public static bool DeathAlready=false;
	private GameObject Gun1;
	public bool isShip;
	public GameObject ShipCamera;
	
	public AudioClip DeathSound;

	public void _Death()
	{
		GlobalSettings.HaveWeapon = false;
		ps.GetComponent<ParticleSystem>().Play();
		if (!isShip)
		{
			GetComponent<CharacterController>().enabled = !enabled;
		}
		else
		if (isShip)
		{
			GetComponent<ShipController>().enabled = !enabled;
			GetComponent<ShipWeaponShoot>().enabled = !enabled;
			ShipCamera.GetComponent<ShipCamera>().enabled = !enabled;
		}
		GetComponent<SpriteRenderer>().enabled = !enabled;
		GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
		GetComponent<BoxCollider2D>().enabled = !enabled;
		
		if (!isShip)
		{
			Gun1 = GameObject.FindWithTag("Gun1");
			Gun1.GetComponent<SpriteRenderer>().enabled = !enabled;
		}
		GlobalSettings.DoubleJumpBuff = false;
		GlobalSettings.PurpleBulletBuff = false;
		GlobalSettings.GreenBulletBuff = false;
		GlobalSettings.DefultBullet = true;
		GlobalSettings.shootdalay = 1;

		

		GetComponent<AudioSource>().PlayOneShot(DeathSound);
		
	}
	void Update()
	{
		if (Death_ == true)
		{
			_Death();
			DeathAlready = true;
			Death_ = false;
		}
		if (DeathAlready == true && Input.GetButtonDown("Fire"))
		{
			SceneManager.LoadScene("LoadingScreen");
			DeathAlready = false;
			GlobalSettings.HP = 3;
			GlobalSettings.HaveWeapon = true;
		}
	}
	
	
	
}
