﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamageCollider : MonoBehaviour {

	public GameObject pt;
	private GameObject AS;
	public AudioClip DeathSound;
	public bool isFlyingRobo;
	public GameObject RangeZone;
	public bool isShip;

	// Use this for initialization
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player" && PlayerDamage.damagedalay<0)
		{
			GlobalSettings.HP--;
			PlayerDamage.TakeDamage = true;
		}
		if (col.tag == "Bullet")
		{
			GlobalSettings.Score += 200;
			GetComponent<BoxCollider2D>().enabled = !enabled;
			GetComponent<SpriteRenderer>().enabled = !enabled;
			if (!isShip)
			{
				GetComponent<RoboMove>().enabled = !enabled;
			}
			pt.GetComponent<ParticleSystem>().Play();
			AS = GameObject.FindWithTag("Audio");
			AS.GetComponent<AudioSource>().PlayOneShot(DeathSound);
			if (isFlyingRobo||isShip)
			{
				Destroy(RangeZone.gameObject);
			}
		}
	}
}
