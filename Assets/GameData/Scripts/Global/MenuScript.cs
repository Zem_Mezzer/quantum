﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour {

	public GameObject StandartOptions;
	public GameObject Options;
	public AudioClip ClickSound;
	public GameObject AS;
	public GameObject About;


	public void StartNewGame()
	{
		AS.GetComponent<AudioSource>().PlayOneShot(ClickSound);
		GlobalSettings.CLevel = "GL1";
		SceneManager.LoadScene("LoadingScreen");
		GlobalSettings.Gun1 = false;
		GlobalSettings.Score = 0;
		GlobalSettings.HP = 3;
	}

	public void Reset()
	{

		string SettingsPath = Application.persistentDataPath + "SaveQM.xml";

		XmlDocument doc_ = new XmlDocument();
		doc_.Load(SettingsPath);
		XmlElement Score_ = (XmlElement)doc_.GetElementsByTagName("Score")[0];
		Score_.InnerText = "0";
		doc_.Save(SettingsPath);

		GlobalSettings.Score = 0;
		AS.GetComponent<AudioSource>().PlayOneShot(ClickSound);
		Options.SetActive(false);
		StandartOptions.SetActive(true);
	}

	public void _Tutorial()
	{
		AS.GetComponent<AudioSource>().PlayOneShot(ClickSound);
		StandartOptions.SetActive(false);
		About.SetActive(true);
	}
	public void Exit()
	{
		AS.GetComponent<AudioSource>().PlayOneShot(ClickSound);
		Application.Quit();
	}
	public void _Options()
	{
		AS.GetComponent<AudioSource>().PlayOneShot(ClickSound);
		Options.SetActive(true);
		StandartOptions.SetActive(false);
	}
	public void back()
	{
		AS.GetComponent<AudioSource>().PlayOneShot(ClickSound);
		Options.SetActive(false);
		StandartOptions.SetActive(true);
		About.SetActive(false);
	}
	public void _800x600()
	{
		AS.GetComponent<AudioSource>().PlayOneShot(ClickSound);
		string SettingsPath = Application.persistentDataPath + "SaveQM.xml";
		XmlDocument doc = new XmlDocument();
		doc.Load(SettingsPath);
		XmlElement ResolutionHeight = (XmlElement)doc.GetElementsByTagName("Height")[0];
		XmlElement ResolutionWidth = (XmlElement)doc.GetElementsByTagName("Width")[0];
		ResolutionWidth.InnerText = "800";
		ResolutionHeight.InnerText = "600";
		Screen.SetResolution(800, 600, false);
		Debug.Log("Resolution Set 800x600");
		doc.Save(SettingsPath);
	}
	public void _1024x768()
	{
		AS.GetComponent<AudioSource>().PlayOneShot(ClickSound);
		string SettingsPath = Application.persistentDataPath + "SaveQM.xml";
		XmlDocument doc = new XmlDocument();
		doc.Load(SettingsPath);
		XmlElement ResolutionHeight = (XmlElement)doc.GetElementsByTagName("Height")[0];
		XmlElement ResolutionWidth = (XmlElement)doc.GetElementsByTagName("Width")[0];
		ResolutionWidth.InnerText = "1024";
		ResolutionHeight.InnerText = "768";
		Screen.SetResolution(1024, 768, false);
		Debug.Log("Resolution Set 1024x768");
		doc.Save(SettingsPath);
	}
	public void _1152x864()
	{
		AS.GetComponent<AudioSource>().PlayOneShot(ClickSound);
		string SettingsPath = Application.persistentDataPath + "SaveQM.xml";
		XmlDocument doc = new XmlDocument();
		doc.Load(SettingsPath);
		XmlElement ResolutionHeight = (XmlElement)doc.GetElementsByTagName("Height")[0];
		XmlElement ResolutionWidth = (XmlElement)doc.GetElementsByTagName("Width")[0];
		ResolutionWidth.InnerText = "1152";
		ResolutionHeight.InnerText = "864";
		Screen.SetResolution(1152, 864, false);
		Debug.Log("Resolution Set 1152x864");
		doc.Save(SettingsPath);
	}


	public void FullScreen (bool isFullScreen)
	{
		Screen.fullScreen = isFullScreen;
	}
	
}
