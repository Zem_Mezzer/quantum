﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;
using System.IO;
using UnityEngine.UI;
using System;
using System.Runtime.Serialization.Formatters.Binary;

public class HighScore : MonoBehaviour {

	public Text _Score;

	void Start()
	{
		string SettingsPath = Application.persistentDataPath + "Save2.qm";
		if (File.Exists(SettingsPath))
		{
			GlobalSettings.SoundVolume = (float)LoadSettings();
		}
	}

	void Update()
	{
		if (GlobalSettings.Score > Convert.ToInt32(LoadScore()))
		{
			SaveScore();
		}
		_Score.text=LoadScore();
		SettingsSave();
	}

	public static void SettingsSave()
	{
		BinaryFormatter formatter = new BinaryFormatter();
		string SettingsPath = Application.persistentDataPath +"Save2.qm";
		FileStream stream = new FileStream(SettingsPath, FileMode.Create);

		formatter.Serialize(stream, GlobalSettings.SoundVolume);
		stream.Close();
	}


	public static double LoadSettings()
	{
		string SettingsPath = Application.persistentDataPath + "Save2.qm";
		if (File.Exists(SettingsPath))
		{
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream stream = new FileStream(SettingsPath, FileMode.Open);


			double Settings = Convert.ToDouble(formatter.Deserialize(stream));
			stream.Close();
			return Settings;

		}
		else
		{
			Debug.Log("Error");
			return 1;
		}

	}


	public static void SaveScore() {

		BinaryFormatter formatter = new BinaryFormatter();
		string Path = Application.persistentDataPath+"Save1.qm";
		FileStream stream = new FileStream(Path,FileMode.Create);

		formatter.Serialize(stream, GlobalSettings.Score);
		stream.Close();
	}
	

	public static string LoadScore()
	{
		string Path = Application.persistentDataPath + "Save1.qm";
		if (File.Exists(Path))
		{
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream stream = new FileStream(Path, FileMode.Open);

			
			string Score = formatter.Deserialize(stream).ToString();
			stream.Close();
			return Score;
			
		}
		else
		{
			Debug.Log("Error");
			return "0";
		}
			
	}
}
