﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using UnityEngine.UI;
public class SaveLoad : MonoBehaviour
{

	public Text Score;
    void Start()
    {

		string SettingsPath = Application.persistentDataPath + "SaveQM.xml";
		if (!File.Exists(SettingsPath))
		{
			XDocument doc = new XDocument();
			XElement Settings = new XElement("Settings");
			XElement Score = new XElement("Score", 0);
			XElement Volume = new XElement("Volume", 1);
			XElement Tutorial = new XElement("Tutorial", true);
			XElement ResolutionWidth = new XElement("Width", 800);
			XElement ResolutionHeight = new XElement("Height", 600);

			Settings.Add(Score);
			Settings.Add(Tutorial);
			Settings.Add(Volume);
			Settings.Add(ResolutionHeight);
			Settings.Add(ResolutionWidth);
			doc.Add(Settings);

			doc.Save(SettingsPath);
		

		}
		else
		{
			
			XmlDocument doc = new XmlDocument();
			doc.Load(SettingsPath);
			XmlElement Volume = (XmlElement)doc.GetElementsByTagName("Volume")[0];
			XmlElement ResolutionWidth = (XmlElement)doc.GetElementsByTagName("Width")[0];
			XmlElement ResolutionHeight = (XmlElement)doc.GetElementsByTagName("Height")[0];
			AudioListener.volume = float.Parse(Volume.InnerText);
			GlobalSettings.SoundVolume = float.Parse(Volume.InnerText);
			Screen.SetResolution(int.Parse(ResolutionWidth.InnerText), int.Parse(ResolutionHeight.InnerText), false);
		}
	}

    
    void Update()
    {
		string SettingsPath = Application.persistentDataPath + "SaveQM.xml";

		XmlDocument doc_ = new XmlDocument();
		doc_.Load(SettingsPath);
		XmlElement Score_ = (XmlElement)doc_.GetElementsByTagName("Score")[0];
		Score.text = Score_.InnerText;

		SaveScore();

		SaveSettings();

	}
	void SaveScore()
	{
		string SettingsPath = Application.persistentDataPath + "SaveQM.xml";

		XmlDocument doc_ = new XmlDocument();
		doc_.Load(SettingsPath);
		XmlElement Score_ = (XmlElement)doc_.GetElementsByTagName("Score")[0];
		if (GlobalSettings.Score > int.Parse(Score_.InnerText))
		{
			Score_.InnerText = GlobalSettings.Score.ToString();
			doc_.Save(SettingsPath);
		}
		
		


	}
	void SaveSettings()
	{
		string SettingsPath = Application.persistentDataPath + "SaveQM.xml";
		XmlDocument doc = new XmlDocument();
		doc.Load(SettingsPath);
		XmlElement Volume = (XmlElement)doc.GetElementsByTagName("Volume")[0];
		Volume.InnerText = GlobalSettings.SoundVolume.ToString();
		doc.Save(SettingsPath);

	}
}
