﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ASyncLoading : MonoBehaviour {

	void Start()
	{
		StartCoroutine(AsyncLoad());
	}

	IEnumerator AsyncLoad()
	{
		AsyncOperation operation = SceneManager.LoadSceneAsync(GlobalSettings.CLevel);
		while (!operation.isDone)
		{
			yield return null;
		}
	}
}
