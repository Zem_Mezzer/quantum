﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseDumpingDisabler : MonoBehaviour
{
    void Update()
    {
		if (Mathf.Abs(Input.GetAxis("Mouse X")) > 0 || Mathf.Abs(Input.GetAxis("Mouse Y")) > 0)
		{
			EventSystem.current.SetSelectedGameObject(null);
			KeysMenuControl.keybordselect = false;
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}
		if (Input.GetButton("Cancel"))
		{
			EventSystem.current.SetSelectedGameObject(null);
		}
	}
}
