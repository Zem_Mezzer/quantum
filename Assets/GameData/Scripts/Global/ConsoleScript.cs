﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConsoleScript : MonoBehaviour {

	public Text tx;
	public string command;
	public GameObject Heart1;
	public GameObject Heart2;
	public GameObject Heart3;
	public GameObject GoldHeart;
	public GameObject GhostHeart;
	public GameObject IronHeart;

	void Update()
	{
		command = tx.text;
		if (Input.GetButtonDown("Submit"))
		{
			Command();
		}
	}
	void Command()
	{
		Debug.Log("Submit");
		if (command == "God")
		{
			GlobalSettings.HP = 9999999;
			IronHeart.GetComponent<Image>().enabled = enabled;
			GoldHeart.GetComponent<Image>().enabled = !enabled;
			GhostHeart.GetComponent<Image>().enabled = !enabled;
			Heart1.GetComponent<Image>().enabled = !enabled;
			Heart2.GetComponent<Image>().enabled = !enabled;
			Heart3.GetComponent<Image>().enabled = !enabled;
			GlobalSettings.Godmode = true;

		}
		if(command == "Heal")
		{
			GlobalSettings.HP++;
		}
		if (command == "Debug_Set_WeaponType_3")
		{
			GlobalSettings.GreenBulletBuff = true;
			GlobalSettings.PurpleBulletBuff = false;
			GlobalSettings.DefultBullet = false;
		}
		if(command == "Debug_Set_WeaponType_2")
		{
			GlobalSettings.GreenBulletBuff = false;
			GlobalSettings.PurpleBulletBuff = true;
			GlobalSettings.DefultBullet = false;
		}
		if (command == "Debug_Set_WeaponType_1")
		{
			GlobalSettings.GreenBulletBuff = false;
			GlobalSettings.PurpleBulletBuff = false;
			GlobalSettings.DefultBullet = true;
		}
		if (command == "DoubleJump_On")
		{
			GlobalSettings.DoubleJumpBuff = true;
		}
		if (command == "DoubleJump_Off")
		{
			GlobalSettings.DoubleJumpBuff = false;
		}
	}

}
