﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawnBuff : MonoBehaviour {

	public GameObject[] buffs;
	private int index;

	void Start () {
		index = Random.Range(0, buffs.Length);

		Instantiate(buffs[index],gameObject.transform.position,gameObject.transform.rotation);
	}
	
	
}
