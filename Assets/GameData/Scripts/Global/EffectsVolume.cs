﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EffectsVolume : MonoBehaviour {
	public Slider slider;

	void Start()
	{
		slider.value = GlobalSettings.SoundVolume;
	}

	public void Volume()
	{
		
		GlobalSettings.SoundVolume = slider.value;
		AudioListener.volume = slider.value;
	}
}
