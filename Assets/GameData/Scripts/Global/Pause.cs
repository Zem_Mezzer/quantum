﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class Pause : MonoBehaviour {

	private bool MenuIsOpen = false;
	public GameObject MenuCanvas;
	public AudioClip ClickSound;
	public bool enableconsole = true;

	public GameObject ConsoleCanvas;
	private bool ConsoleIsOpen = false;
	public GameObject DeathScreen;

	void Update () {

		//if (Death.Death_)
		//{
		//	DeathScreen.SetActive(true);
		//}

		if (Input.GetButtonDown("Console") && ConsoleIsOpen == false&&enableconsole)
		{
			Time.timeScale = 0;
			ConsoleIsOpen = true;
			ConsoleCanvas.SetActive(true);
		}
		else
		if (Input.GetButtonDown("Console") && ConsoleIsOpen == true || Input.GetButtonDown("Pause") && ConsoleIsOpen == true&&enableconsole)
		{
			Time.timeScale = 1;
			ConsoleIsOpen = false;
			ConsoleCanvas.SetActive(false);
		}
		else
		if (Input.GetButtonDown("Pause") &&MenuIsOpen==false)
		{
			Time.timeScale = 0;
			MenuIsOpen = true;
			MenuCanvas.SetActive(true);
			EventSystem.current.SetSelectedGameObject(null);
			KeysMenuControl.keybordselect = false;
		}
		else
		if (Input.GetButtonDown("Pause") && MenuIsOpen == true)
		{
			Time.timeScale = 1;
			MenuIsOpen = false;
			MenuCanvas.SetActive(false);
			EventSystem.current.SetSelectedGameObject(null);
			KeysMenuControl.keybordselect = false;
		}
	}

	public void Restart()
	{
		EventSystem.current.SetSelectedGameObject(null);
		GameObject AS = GameObject.FindWithTag("Audio");
		AS.GetComponent<AudioSource>().PlayOneShot(ClickSound);
		SceneManager.LoadScene("LoadingScreen");
		GlobalSettings.HP = 3;
		GlobalSettings.HaveWeapon = true;
		Death.DeathAlready = false;
		KeysMenuControl.keybordselect = false;
		GlobalSettings.GreenBulletBuff = false;
		GlobalSettings.PurpleBulletBuff = false;
		GlobalSettings.DefultBullet = true;
	}

	public void Resume()
	{

		EventSystem.current.SetSelectedGameObject(null);
		KeysMenuControl.keybordselect = false;
		GameObject AS = GameObject.FindWithTag("Audio");
		AS.GetComponent<AudioSource>().PlayOneShot(ClickSound);
		MenuIsOpen = false;
		Time.timeScale = 1;
		MenuCanvas.SetActive(false);

	}
	public void BackToMenu()
	{
		EventSystem.current.SetSelectedGameObject(null);
		KeysMenuControl.keybordselect = false;
		Death.DeathAlready = false;
		GameObject AS = GameObject.FindWithTag("Audio");
		AS.GetComponent<AudioSource>().PlayOneShot(ClickSound);
		Time.timeScale = 1;
		GlobalSettings.GreenBulletBuff = false;
		GlobalSettings.PurpleBulletBuff = false;
		GlobalSettings.DefultBullet = true;
		GlobalSettings.CLevel = "MainMenu";
		SceneManager.LoadScene("LoadingScreen");
		
	}
}
