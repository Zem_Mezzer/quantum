﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalSettings : MonoBehaviour {

	public static string CLevel="GL1";
	public static int HP = 3;

	public static float EffectsVolume=1;
	public static float SoundVolume=1;

	public static bool Gun1=false;

	public static bool EndStopShoot = false;

	public static bool DoubleJumpBuff = false;

	public static bool PurpleBulletBuff = false;

	public static bool GreenBulletBuff = false;

	public static bool DefultBullet = true;

	public static float shootdalay;

	public static bool Godmode = false;

	public static bool HaveWeapon = false;

	public static int Score = 0;

	public static bool Tutorial = true;

	public static bool JoystickConnected = false;

	//public static int Timer_For_Stas = 0;

	//public static bool timerwork = true;

	public static bool PlayerDamageBool = false;
}
