﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{


	public float PlayerSpeed;
	private float InputValue;
	private Rigidbody2D rb;
	public float JumpForce;
	public Animator anim;
	public bool Facing = true;

	public float jumptms;
	//Jumping
	public static int JumpTimes=1;
	public bool isGrounded;
	public Transform GroundCheck;
	public float CheckRadius;
	public LayerMask WhatIsGround;
	//
	public AudioClip JumpSound;

	public GameObject Gun;
	public bool FirstLevelWeaponDeact = false;
	void Start()
	{
		GlobalSettings.EndStopShoot = false;
		rb = GetComponent<Rigidbody2D>();

		if (GlobalSettings.Gun1 == true)
		{
			Gun.GetComponent<SpriteRenderer>().enabled = enabled;
		}
	}
	void FixedUpdate()
	{

		jumptms = JumpTimes;
		InputValue = Input.GetAxisRaw("Horizontal");
		rb.velocity = new Vector2(PlayerSpeed * InputValue, rb.velocity.y);
		anim.SetFloat("Speed", Mathf.Abs(InputValue));
		if (GlobalSettings.DoubleJumpBuff == false && isGrounded)
		{
			JumpTimes = 1;
		}
		if (GlobalSettings.DoubleJumpBuff == true && isGrounded)
		{
			JumpTimes = 2;
		}
		isGrounded = Physics2D.OverlapCircle(GroundCheck.position, CheckRadius, WhatIsGround);

	}
	void Update()
	{
		if (GlobalSettings.Gun1&&GlobalSettings.HaveWeapon)
		{
			Gun.GetComponent<SpriteRenderer>().enabled = enabled;
		}

		if (Input.GetButtonDown("Jump")&&isGrounded)
		{
				GetComponent<AudioSource>().PlayOneShot(JumpSound);
				rb.velocity = Vector2.up * JumpForce;
				isGrounded = false;
				JumpTimes--;
		}
		if (Facing == false && InputValue > 0)
		{
			Flip();
		}
		if (Facing == true && InputValue < 0)
		{
			Flip();
		}

	}
	void Flip()
	{
		Facing = !Facing;

		transform.Rotate(0f, 180f, 0f);
	}
}
//void OnTriggerEnter2D(Collider2D col)
//{
//	if (GlobalSettings.DoubleJumpBuff == false && col.gameObject.layer == 8)
//	{
//		JumpTimes = 1;
//	}
//	else
//		if (GlobalSettings.DoubleJumpBuff == true && col.gameObject.layer == 8)
//	{
//		JumpTimes = 2;
//	}
//}
