﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour {

	public float Speed;
	public float HSpeed;

	float ShipSize;
	

	void Update()
	{
		Vector3 position = transform.position;

		position.y += Input.GetAxisRaw("Vertical") * Speed * Time.deltaTime;
		
		position.x += Input.GetAxisRaw("Horizontal") * HSpeed * Time.deltaTime;
		

		//if (position.y+ShipSize > Camera.main.orthographicSize)
		//{
		//	position.y = Camera.main.orthographicSize-ShipSize;
		//}
		//if (position.y - ShipSize < -Camera.main.orthographicSize)
		//{
		//	position.y = -Camera.main.orthographicSize + ShipSize;
		//}
		//float ScreenRat = (float)Screen.width / (float)Screen.height;
		//float WidthOrtho = Camera.main.orthographicSize * ScreenRat;
		//if (position.x + ShipSize > WidthOrtho)
		//{
		//	position.x = WidthOrtho - ShipSize;
		//}
		//if (position.x - ShipSize < -WidthOrtho)
		//{
		//	position.x = -WidthOrtho + ShipSize;
		//}
		transform.position = position;
	}
}
