﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class KeysMenuControl : MonoBehaviour
{
	public static bool keybordselect = false;

	//void Start()
	//{
	//	EventSystem.current.SetSelectedGameObject(this.gameObject);
	//	keybordselect = true;
	//	Cursor.lockState = CursorLockMode.Locked;
	//	Cursor.visible = false;
	//}

    void Update()
    {
		if (Mathf.Abs(Input.GetAxis("Horizontal"))>0 && !keybordselect || Mathf.Abs(Input.GetAxis("Vertical")) > 0 && !keybordselect || Input.GetButton("Vertical") && !keybordselect || Input.GetButton("Horizontal") && !keybordselect)
		{
			EventSystem.current.SetSelectedGameObject(this.gameObject);
			keybordselect = true;
			Cursor.lockState=CursorLockMode.Locked;
			Cursor.visible = false;

		}
		
		if (Input.GetButton("Submit"))
		{
			EventSystem.current.SetSelectedGameObject(null);
			keybordselect = false;
		}
		

    }
}
