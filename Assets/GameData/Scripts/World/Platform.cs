﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour {

	public GameObject FirstWayPoint;
	public GameObject SecondWayPoint;
	public float speed;
	private bool LeftOrRight = true;
	void Update()
	{
		if (LeftOrRight == true)
		{
			transform.position = Vector2.MoveTowards(transform.position, FirstWayPoint.transform.position, speed * Time.deltaTime);
			if (Vector2.Distance(transform.position, FirstWayPoint.transform.position) < 0.1f)
			{
				LeftOrRight = false;
			}
		}
		else
			if (LeftOrRight == false)
		{
			transform.position = Vector2.MoveTowards(transform.position, SecondWayPoint.transform.position, speed * Time.deltaTime);
			if (Vector2.Distance(transform.position, SecondWayPoint.transform.position) < 0.1f)
			{
				LeftOrRight = true;
			}
		}
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			Debug.Log("Collider");
			GameObject Player = GameObject.FindWithTag("Player");
			Player.transform.parent = this.gameObject.transform;
		}
	}
	void OnCollisionExit2D(Collision2D col)
	{
		if (col.gameObject.tag == "Player")
		{
			GameObject Player = GameObject.FindWithTag("Player");
			Player.transform.parent = null;
		}
	}

}
