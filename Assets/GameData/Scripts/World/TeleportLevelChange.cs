﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleportLevelChange : MonoBehaviour {

	private GameObject Pl;
	private bool starttimer;
	private float timer=3;
	public string LevelToLoad;
	public GameObject pt;
	//public bool isDev;

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player")
		{
			//if (isDev)
			//{
			//	GlobalSettings.timerwork = false;
			//}
			Pl = GameObject.FindWithTag("Player");
			Pl.GetComponent<SpriteRenderer>().enabled = !enabled;
			Pl.GetComponent<CharacterController>().enabled = !enabled;
			Pl.GetComponent<BoxCollider2D>().enabled = !enabled;
			Pl.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
			Pl = GameObject.FindWithTag("Gun1");
			Pl.GetComponent<SpriteRenderer>().enabled = !enabled;
			starttimer = true;
			pt.GetComponent<ParticleSystem>().Play();
			GlobalSettings.EndStopShoot = true;
		}
	}
	void Update()
	{
		if (timer > 0&&starttimer)
		{
			timer -= Time.deltaTime;
			if (timer <= 0)
			{
				GlobalSettings.CLevel = LevelToLoad;
				SceneManager.LoadScene("LoadingScreen");
			}
		}
	}
}
