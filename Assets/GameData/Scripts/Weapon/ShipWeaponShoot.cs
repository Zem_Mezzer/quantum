﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipWeaponShoot : MonoBehaviour {

	public GameObject DefultBullet;
	public GameObject PurplePullet;
	public Transform ShootPoint;
	public float Dalaytoshoot=1;
	public AudioClip ShootSound;
	

	void Update () {
		if (Input.GetButtonDown("Fire")&&GlobalSettings.Gun1)
		{
			if (GlobalSettings.DefultBullet && Dalaytoshoot < 0)
			{
				Instantiate(DefultBullet, ShootPoint.transform.position, ShootPoint.transform.rotation);
				Dalaytoshoot = 1;
				GetComponent<AudioSource>().PlayOneShot(ShootSound);
			}
			else
			if (GlobalSettings.PurpleBulletBuff)
			{
				Instantiate(PurplePullet, ShootPoint.transform.position, ShootPoint.transform.rotation);
				GetComponent<AudioSource>().PlayOneShot(ShootSound);
			}
			else
			if (GlobalSettings.GreenBulletBuff && Dalaytoshoot<0)
			{
				Instantiate(DefultBullet, ShootPoint.transform.position, ShootPoint.transform.rotation);
				Dalaytoshoot = 1;
				GetComponent<AudioSource>().PlayOneShot(ShootSound);
			}
		}
		if (Dalaytoshoot > 0)
		{
			Dalaytoshoot-=Time.deltaTime;
		}
	}
}
