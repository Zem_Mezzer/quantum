﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectWeapon : MonoBehaviour {
	public AudioClip pickupsound;
	private GameObject AS;
	public bool isGun1;

	private GameObject Gun1;
	void Start () {
		Gun1 = GameObject.FindWithTag("Gun1");
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "Player" && isGun1)
		{
			Gun1.GetComponent<SpriteRenderer>().enabled = enabled;
			GlobalSettings.Gun1 = true;
			GlobalSettings.HaveWeapon = true;
			AS = GameObject.FindWithTag("Audio");
			AS.GetComponent<AudioSource>().PlayOneShot(pickupsound);
			GlobalSettings.Score += 100;
			Destroy(this.gameObject);
			
		}
	}
}
