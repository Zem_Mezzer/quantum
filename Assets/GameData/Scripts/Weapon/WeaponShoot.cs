﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponShoot : MonoBehaviour {

	public GameObject Defultbullet;
	public GameObject PurpleBullet;
	public GameObject GreenBullet;

	public Transform Gun1shootPoint;
	public AudioClip ShootSound;
	private bool isshooting;

	void Start()
	{
		if (GlobalSettings.DefultBullet)
			GlobalSettings.shootdalay = 1;
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire") && GlobalSettings.Gun1&&GlobalSettings.HaveWeapon)
		{
			if (GlobalSettings.EndStopShoot == false && GlobalSettings.shootdalay == 1 && GlobalSettings.DefultBullet)
			{
				ShootDefultBullet();
				isshooting = true;
				GlobalSettings.shootdalay -= 0.001f;
			}
			if (GlobalSettings.EndStopShoot == false && GlobalSettings.PurpleBulletBuff)
			{
				ShootPurpleBullet();
				isshooting = true;
			}
			if (GlobalSettings.EndStopShoot == false && GlobalSettings.shootdalay == 1 && GlobalSettings.GreenBulletBuff)
			{
				ShootGreenBullet();
				isshooting = true;
				GlobalSettings.shootdalay -= 0.001f;
			}
		}
		if (GlobalSettings.shootdalay < 1&&GlobalSettings.DefultBullet|| GlobalSettings.shootdalay < 1 && GlobalSettings.GreenBulletBuff)
		{
			GlobalSettings.shootdalay -= Time.deltaTime;
			if (GlobalSettings.shootdalay <= 0)
			{
				GlobalSettings.shootdalay = 1;
			}
		}

		
	}

	void ShootDefultBullet()
	{
		GetComponent<AudioSource>().PlayOneShot(ShootSound);
		Instantiate(Defultbullet, Gun1shootPoint.position, Gun1shootPoint.rotation);
	}
	void ShootPurpleBullet()
	{
		GetComponent<AudioSource>().PlayOneShot(ShootSound);
		Instantiate(PurpleBullet, Gun1shootPoint.position, Gun1shootPoint.rotation);
	}
	void ShootGreenBullet()
	{
		GetComponent<AudioSource>().PlayOneShot(ShootSound);
		Instantiate(GreenBullet, Gun1shootPoint.position, Gun1shootPoint.rotation);
	}
}
