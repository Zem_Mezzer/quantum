﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShipEnd : MonoBehaviour {

	public GameObject PlayerShip;
	public string LeveltoLoad;
	
	void End () {
		GlobalSettings.CLevel = LeveltoLoad;
		SceneManager.LoadScene("LoadingScreen");
	}
	
	
	void ShipVanish () {
		PlayerShip.GetComponent<SpriteRenderer>().enabled = !enabled;
		PlayerShip.GetComponent<ShipController>().enabled = !enabled;
		PlayerShip.GetComponent<ShipWeaponShoot>().enabled = !enabled;
	}
}
