﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipStart : MonoBehaviour {

	public GameObject Player;
	public GameObject Camera;

	void StartControl()
	{
		Player.GetComponent<ShipController>().enabled = enabled;
		Camera.GetComponent<ShipCamera>().enabled = enabled;
		Destroy(this.gameObject);
	}
	
}
