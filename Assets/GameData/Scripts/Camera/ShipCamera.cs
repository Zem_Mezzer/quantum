﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipCamera : MonoBehaviour {

	public float speed;

	void Update()
	{
		Vector3 position = transform.position;
		position.x += speed * Time.deltaTime;
		transform.position = position;
	}
}
