﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraStop : MonoBehaviour {

	public GameObject Camera;
	public GameObject Wall;
	public GameObject EndMapAnim;

	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "CameraWall")
		{
			EndMapAnim.SetActive(true);
			EndMapAnim.GetComponent<Animator>().Play("ShipEndAnim");
			Camera.GetComponent<ShipCamera>().enabled = !enabled;
			Destroy(Wall);
		}
	}

}
