﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{

	public Text Score_Value;

    void Start()
    {
		Score_Value.text = GlobalSettings.Score.ToString();
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetButtonDown("Submit"))
		{
			GlobalSettings.CLevel = "MainMenu";
			GlobalSettings.GreenBulletBuff = false;
			GlobalSettings.PurpleBulletBuff = false;
			GlobalSettings.DefultBullet = true;
			SceneManager.LoadScene("LoadingScreen");
		}
    }
}
